# GIS gestione manutenzioni



## Il progetto

La finalità del progetto è la gestione via GIS (QGIS) delle manutenzioni sul territorio.
E' rivolto principalmente alla pubbliche amministrazioni e a tutti gli enti che hanno la necessità di tenere traccia delle manutenzioni eseguite.

Le soluzioni previste sono 3. Vengono sviluppate le versioni 1 e 3, poichè la versione 2 è realizzabile con la versione 1 semplicemente utilizzando la stessa impostazione del database, ma variando semplicemente il flusso dei dati.

Tutte le versioni si basano su un database che prevede una tabella "oggetti". In questa tabella vengono registrare le geometrie.

La seconda tabella raccoglie solamente i dati delle manutenzioni.

Le tabelle sono collegate con relazione uno-a-molti (1:m); la tabella "oggetti" è la tabella padre, mentre l'altra è la tabella figlio.

La **soluzione 1** si differenza dalla 3 nella tabella delle manutenzioni.

Per la soluzione 1 si prevede che per ogni evento riguardante l'oggetto di manutenzione (segnalazione, programmazione ed esecuzione) venga creata una nuova riga dati.

Per la **soluzione 3** gli eventi vengono registrati nella medesima riga, quindi un evento si apre e si chiude sulla stessa registrazione.

La **soluzione 2**, per la quale si utilizza l'impostazione della soluzione 1, non prevede la creazione di una riga dati per ogni evento, ma viene modificata quella esistente, perdendo però lo storico.