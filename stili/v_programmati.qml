<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis minScale="100000000" simplifyMaxScale="1" simplifyDrawingHints="0" readOnly="0" version="3.36.0-Maidenhead" simplifyDrawingTol="1" styleCategories="AllStyleCategories" maxScale="0" hasScaleBasedVisibilityFlag="0" labelsEnabled="0" simplifyAlgorithm="0" simplifyLocal="1" symbologyReferenceScale="-1">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal startField="" durationUnit="min" startExpression="" accumulate="0" fixedDuration="0" limitMode="0" endExpression="" mode="0" endField="" enabled="0" durationField="id_ogg">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation zoffset="0" binding="Centroid" extrusion="0" clamping="Terrain" type="IndividualFeatures" zscale="1" symbology="Line" respectLayerSymbol="1" extrusionEnabled="0" showMarkerSymbolInSurfacePlots="0">
    <data-defined-properties>
      <Option type="Map">
        <Option name="name" type="QString" value=""/>
        <Option name="properties"/>
        <Option name="type" type="QString" value="collection"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol clip_to_extent="1" alpha="1" name="" type="line" frame_rate="10" force_rhr="0" is_animated="0">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer id="{55237986-0572-431e-a442-8456597942cf}" pass="0" locked="0" enabled="1" class="SimpleLine">
          <Option type="Map">
            <Option name="align_dash_pattern" type="QString" value="0"/>
            <Option name="capstyle" type="QString" value="square"/>
            <Option name="customdash" type="QString" value="5;2"/>
            <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="customdash_unit" type="QString" value="MM"/>
            <Option name="dash_pattern_offset" type="QString" value="0"/>
            <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
            <Option name="draw_inside_polygon" type="QString" value="0"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="line_color" type="QString" value="232,113,141,255,rgb:0.90980392156862744,0.44313725490196076,0.55294117647058827,1"/>
            <Option name="line_style" type="QString" value="solid"/>
            <Option name="line_width" type="QString" value="0.6"/>
            <Option name="line_width_unit" type="QString" value="MM"/>
            <Option name="offset" type="QString" value="0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="ring_filter" type="QString" value="0"/>
            <Option name="trim_distance_end" type="QString" value="0"/>
            <Option name="trim_distance_end_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_end_unit" type="QString" value="MM"/>
            <Option name="trim_distance_start" type="QString" value="0"/>
            <Option name="trim_distance_start_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_start_unit" type="QString" value="MM"/>
            <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
            <Option name="use_custom_dash" type="QString" value="0"/>
            <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol clip_to_extent="1" alpha="1" name="" type="fill" frame_rate="10" force_rhr="0" is_animated="0">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer id="{ecb2ee6d-35b8-4b8f-a864-7a2fc202a7be}" pass="0" locked="0" enabled="1" class="SimpleFill">
          <Option type="Map">
            <Option name="border_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="color" type="QString" value="232,113,141,255,rgb:0.90980392156862744,0.44313725490196076,0.55294117647058827,1"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="166,81,101,255,rgb:0.64985122453650723,0.31651789120317386,0.39496452277409017,1"/>
            <Option name="outline_style" type="QString" value="solid"/>
            <Option name="outline_width" type="QString" value="0.2"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="style" type="QString" value="solid"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol clip_to_extent="1" alpha="1" name="" type="marker" frame_rate="10" force_rhr="0" is_animated="0">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer id="{c1c714cf-6bb2-419d-bb7f-554bc0a7db10}" pass="0" locked="0" enabled="1" class="SimpleMarker">
          <Option type="Map">
            <Option name="angle" type="QString" value="0"/>
            <Option name="cap_style" type="QString" value="square"/>
            <Option name="color" type="QString" value="232,113,141,255,rgb:0.90980392156862744,0.44313725490196076,0.55294117647058827,1"/>
            <Option name="horizontal_anchor_point" type="QString" value="1"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="name" type="QString" value="diamond"/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="166,81,101,255,rgb:0.64985122453650723,0.31651789120317386,0.39496452277409017,1"/>
            <Option name="outline_style" type="QString" value="solid"/>
            <Option name="outline_width" type="QString" value="0.2"/>
            <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="scale_method" type="QString" value="diameter"/>
            <Option name="size" type="QString" value="3"/>
            <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="size_unit" type="QString" value="MM"/>
            <Option name="vertical_anchor_point" type="QString" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 toleranceUnitScale="3x:0,0,0,0,0,0" type="pointCluster" tolerance="3" forceraster="0" toleranceUnit="MM" referencescale="-1" enableorderby="0" symbollevels="0">
    <renderer-v2 type="singleSymbol" forceraster="0" referencescale="-1" enableorderby="0" symbollevels="0">
      <symbols>
        <symbol clip_to_extent="1" alpha="1" name="0" type="marker" frame_rate="10" force_rhr="0" is_animated="0">
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
          <layer id="{25c5ce79-dde5-4aa8-9100-f8a95224d36e}" pass="0" locked="0" enabled="1" class="SimpleMarker">
            <Option type="Map">
              <Option name="angle" type="QString" value="0"/>
              <Option name="cap_style" type="QString" value="square"/>
              <Option name="color" type="QString" value="255,221,1,255,hsv:0.14444444444444443,0.99607843137254903,1,1"/>
              <Option name="horizontal_anchor_point" type="QString" value="1"/>
              <Option name="joinstyle" type="QString" value="bevel"/>
              <Option name="name" type="QString" value="triangle"/>
              <Option name="offset" type="QString" value="0,0"/>
              <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="offset_unit" type="QString" value="MM"/>
              <Option name="outline_color" type="QString" value="35,35,35,255,rgb:0.13725490196078433,0.13725490196078433,0.13725490196078433,1"/>
              <Option name="outline_style" type="QString" value="solid"/>
              <Option name="outline_width" type="QString" value="0"/>
              <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="outline_width_unit" type="QString" value="MM"/>
              <Option name="scale_method" type="QString" value="diameter"/>
              <Option name="size" type="QString" value="3"/>
              <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="size_unit" type="QString" value="MM"/>
              <Option name="vertical_anchor_point" type="QString" value="1"/>
            </Option>
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" type="QString" value=""/>
                <Option name="properties"/>
                <Option name="type" type="QString" value="collection"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </symbols>
      <rotation/>
      <sizescale/>
    </renderer-v2>
    <symbol clip_to_extent="1" alpha="1" name="centerSymbol" type="marker" frame_rate="10" force_rhr="0" is_animated="0">
      <data_defined_properties>
        <Option type="Map">
          <Option name="name" type="QString" value=""/>
          <Option name="properties"/>
          <Option name="type" type="QString" value="collection"/>
        </Option>
      </data_defined_properties>
      <layer id="{2af1982d-6fb2-480f-8b93-bb0a96f074ce}" pass="0" locked="0" enabled="1" class="SimpleMarker">
        <Option type="Map">
          <Option name="angle" type="QString" value="0"/>
          <Option name="cap_style" type="QString" value="square"/>
          <Option name="color" type="QString" value="255,221,1,255,hsv:0.14444444444444443,0.99607843137254903,1,1"/>
          <Option name="horizontal_anchor_point" type="QString" value="1"/>
          <Option name="joinstyle" type="QString" value="bevel"/>
          <Option name="name" type="QString" value="circle"/>
          <Option name="offset" type="QString" value="0,0"/>
          <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          <Option name="offset_unit" type="QString" value="MM"/>
          <Option name="outline_color" type="QString" value="35,35,35,255,rgb:0.13725490196078433,0.13725490196078433,0.13725490196078433,1"/>
          <Option name="outline_style" type="QString" value="solid"/>
          <Option name="outline_width" type="QString" value="0"/>
          <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          <Option name="outline_width_unit" type="QString" value="MM"/>
          <Option name="scale_method" type="QString" value="diameter"/>
          <Option name="size" type="QString" value="4"/>
          <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          <Option name="size_unit" type="QString" value="MM"/>
          <Option name="vertical_anchor_point" type="QString" value="1"/>
        </Option>
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
      </layer>
      <layer id="{b63e27b5-cf80-431e-b1d2-1aa24aecf592}" pass="0" locked="0" enabled="1" class="FontMarker">
        <Option type="Map">
          <Option name="angle" type="QString" value="0"/>
          <Option name="chr" type="QString" value="A"/>
          <Option name="color" type="QString" value="0,0,0,255,hsv:0,0,0,1"/>
          <Option name="font" type="QString" value="DejaVu Sans"/>
          <Option name="font_style" type="QString" value=""/>
          <Option name="horizontal_anchor_point" type="QString" value="1"/>
          <Option name="joinstyle" type="QString" value="miter"/>
          <Option name="offset" type="QString" value="0,-0.40000000000000002"/>
          <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          <Option name="offset_unit" type="QString" value="MM"/>
          <Option name="outline_color" type="QString" value="255,255,255,255,rgb:1,1,1,1"/>
          <Option name="outline_width" type="QString" value="0"/>
          <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          <Option name="outline_width_unit" type="QString" value="MM"/>
          <Option name="size" type="QString" value="3.2"/>
          <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          <Option name="size_unit" type="QString" value="MM"/>
          <Option name="vertical_anchor_point" type="QString" value="1"/>
        </Option>
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties" type="Map">
              <Option name="char" type="Map">
                <Option name="active" type="bool" value="true"/>
                <Option name="expression" type="QString" value="@cluster_size"/>
                <Option name="type" type="int" value="3"/>
              </Option>
            </Option>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
      </layer>
    </symbol>
  </renderer-v2>
  <selection mode="Default">
    <selectionColor invalid="1"/>
    <selectionSymbol>
      <symbol clip_to_extent="1" alpha="1" name="" type="marker" frame_rate="10" force_rhr="0" is_animated="0">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer id="{7596ffd1-43dd-4fd7-bfc1-1daef45e01d5}" pass="0" locked="0" enabled="1" class="SimpleMarker">
          <Option type="Map">
            <Option name="angle" type="QString" value="0"/>
            <Option name="cap_style" type="QString" value="square"/>
            <Option name="color" type="QString" value="255,0,0,255,rgb:1,0,0,1"/>
            <Option name="horizontal_anchor_point" type="QString" value="1"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="name" type="QString" value="circle"/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="35,35,35,255,rgb:0.13725490196078433,0.13725490196078433,0.13725490196078433,1"/>
            <Option name="outline_style" type="QString" value="solid"/>
            <Option name="outline_width" type="QString" value="0"/>
            <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="scale_method" type="QString" value="diameter"/>
            <Option name="size" type="QString" value="2"/>
            <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="size_unit" type="QString" value="MM"/>
            <Option name="vertical_anchor_point" type="QString" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </selectionSymbol>
  </selection>
  <customproperties>
    <Option type="Map">
      <Option name="QFieldSync/action" type="QString" value="no_action"/>
      <Option name="QFieldSync/attachment_naming" type="QString" value="{&quot;foto&quot;: &quot;'DCIM/v-programmati_' || format_date(now(),'yyyyMMddhhmmsszzz') || '.{extension}'&quot;}"/>
      <Option name="QFieldSync/cloud_action" type="QString" value="no_action"/>
      <Option name="QFieldSync/geometry_locked_expression" type="QString" value=""/>
      <Option name="QFieldSync/photo_naming" type="QString" value="{&quot;foto&quot;: &quot;'DCIM/v-programmati_' || format_date(now(),'yyyyMMddhhmmsszzz') || '.{extension}'&quot;}"/>
      <Option name="QFieldSync/relationship_maximum_visible" type="QString" value="{}"/>
      <Option name="QFieldSync/tracking_distance_requirement_minimum_meters" type="int" value="30"/>
      <Option name="QFieldSync/tracking_erroneous_distance_safeguard_maximum_meters" type="int" value="1"/>
      <Option name="QFieldSync/tracking_measurement_type" type="int" value="0"/>
      <Option name="QFieldSync/tracking_time_requirement_interval_seconds" type="int" value="30"/>
      <Option name="dualview/previewExpressions" type="List">
        <Option type="QString" value="&quot;oggetto&quot;"/>
      </Option>
      <Option name="embeddedWidgets/count" type="int" value="0"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory enabled="0" height="15" opacity="1" backgroundColor="#ffffff" barWidth="5" minScaleDenominator="0" diagramOrientation="Up" maxScaleDenominator="1e+08" penColor="#000000" rotationOffset="270" sizeType="MM" penWidth="0" spacingUnitScale="3x:0,0,0,0,0,0" penAlpha="255" spacingUnit="MM" lineSizeScale="3x:0,0,0,0,0,0" scaleDependency="Area" minimumSize="0" backgroundAlpha="255" showAxis="1" direction="0" scaleBasedVisibility="0" lineSizeType="MM" sizeScale="3x:0,0,0,0,0,0" width="15" labelPlacementMethod="XHeight" spacing="5">
      <fontProperties bold="0" style="" description="Ubuntu,10,-1,5,50,0,0,0,0,0" italic="0" underline="0" strikethrough="0"/>
      <attribute colorOpacity="1" label="" color="#000000" field=""/>
      <axisSymbol>
        <symbol clip_to_extent="1" alpha="1" name="" type="line" frame_rate="10" force_rhr="0" is_animated="0">
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
          <layer id="{50754950-9d87-429b-abda-040213e9cc94}" pass="0" locked="0" enabled="1" class="SimpleLine">
            <Option type="Map">
              <Option name="align_dash_pattern" type="QString" value="0"/>
              <Option name="capstyle" type="QString" value="square"/>
              <Option name="customdash" type="QString" value="5;2"/>
              <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="customdash_unit" type="QString" value="MM"/>
              <Option name="dash_pattern_offset" type="QString" value="0"/>
              <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
              <Option name="draw_inside_polygon" type="QString" value="0"/>
              <Option name="joinstyle" type="QString" value="bevel"/>
              <Option name="line_color" type="QString" value="35,35,35,255,rgb:0.13725490196078433,0.13725490196078433,0.13725490196078433,1"/>
              <Option name="line_style" type="QString" value="solid"/>
              <Option name="line_width" type="QString" value="0.26"/>
              <Option name="line_width_unit" type="QString" value="MM"/>
              <Option name="offset" type="QString" value="0"/>
              <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="offset_unit" type="QString" value="MM"/>
              <Option name="ring_filter" type="QString" value="0"/>
              <Option name="trim_distance_end" type="QString" value="0"/>
              <Option name="trim_distance_end_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="trim_distance_end_unit" type="QString" value="MM"/>
              <Option name="trim_distance_start" type="QString" value="0"/>
              <Option name="trim_distance_start_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="trim_distance_start_unit" type="QString" value="MM"/>
              <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
              <Option name="use_custom_dash" type="QString" value="0"/>
              <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            </Option>
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" type="QString" value=""/>
                <Option name="properties"/>
                <Option name="type" type="QString" value="collection"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings zIndex="0" linePlacementFlags="18" showAll="1" placement="0" priority="0" obstacle="0" dist="0">
    <properties>
      <Option type="Map">
        <Option name="name" type="QString" value=""/>
        <Option name="properties"/>
        <Option name="type" type="QString" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend showLabelLegend="0" type="default-vector"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field configurationFlags="NoFlag" name="id_ogg">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="toponomastica">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="rifciv">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="oggetto">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="foto">
      <editWidget type="ExternalResource">
        <config>
          <Option type="Map">
            <Option name="DocumentViewer" type="int" value="1"/>
            <Option name="DocumentViewerHeight" type="int" value="0"/>
            <Option name="DocumentViewerWidth" type="int" value="200"/>
            <Option name="FileWidget" type="bool" value="true"/>
            <Option name="FileWidgetButton" type="bool" value="true"/>
            <Option name="FileWidgetFilter" type="QString" value=""/>
            <Option name="PropertyCollection" type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
            <Option name="RelativeStorage" type="int" value="0"/>
            <Option name="StorageAuthConfigId" type="QString" value=""/>
            <Option name="StorageMode" type="int" value="0"/>
            <Option name="StorageType" type="QString" value=""/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="data">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="nome">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="intervento">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" type="bool" value="true"/>
            <Option name="UseHtml" type="bool" value="false"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="scadenza">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" index="0" field="id_ogg"/>
    <alias name="" index="1" field="toponomastica"/>
    <alias name="" index="2" field="rifciv"/>
    <alias name="" index="3" field="oggetto"/>
    <alias name="" index="4" field="foto"/>
    <alias name="" index="5" field="data"/>
    <alias name="" index="6" field="nome"/>
    <alias name="" index="7" field="intervento"/>
    <alias name="" index="8" field="scadenza"/>
  </aliases>
  <splitPolicies>
    <policy field="id_ogg" policy="Duplicate"/>
    <policy field="toponomastica" policy="Duplicate"/>
    <policy field="rifciv" policy="Duplicate"/>
    <policy field="oggetto" policy="Duplicate"/>
    <policy field="foto" policy="DefaultValue"/>
    <policy field="data" policy="Duplicate"/>
    <policy field="nome" policy="Duplicate"/>
    <policy field="intervento" policy="DefaultValue"/>
    <policy field="scadenza" policy="Duplicate"/>
  </splitPolicies>
  <defaults>
    <default expression="" applyOnUpdate="0" field="id_ogg"/>
    <default expression="" applyOnUpdate="0" field="toponomastica"/>
    <default expression="" applyOnUpdate="0" field="rifciv"/>
    <default expression="" applyOnUpdate="0" field="oggetto"/>
    <default expression="" applyOnUpdate="0" field="foto"/>
    <default expression="" applyOnUpdate="0" field="data"/>
    <default expression="" applyOnUpdate="0" field="nome"/>
    <default expression="" applyOnUpdate="0" field="intervento"/>
    <default expression="" applyOnUpdate="0" field="scadenza"/>
  </defaults>
  <constraints>
    <constraint unique_strength="0" constraints="0" notnull_strength="0" field="id_ogg" exp_strength="0"/>
    <constraint unique_strength="0" constraints="0" notnull_strength="0" field="toponomastica" exp_strength="0"/>
    <constraint unique_strength="0" constraints="0" notnull_strength="0" field="rifciv" exp_strength="0"/>
    <constraint unique_strength="0" constraints="0" notnull_strength="0" field="oggetto" exp_strength="0"/>
    <constraint unique_strength="0" constraints="0" notnull_strength="0" field="foto" exp_strength="0"/>
    <constraint unique_strength="0" constraints="0" notnull_strength="0" field="data" exp_strength="0"/>
    <constraint unique_strength="0" constraints="0" notnull_strength="0" field="nome" exp_strength="0"/>
    <constraint unique_strength="0" constraints="0" notnull_strength="0" field="intervento" exp_strength="0"/>
    <constraint unique_strength="0" constraints="0" notnull_strength="0" field="scadenza" exp_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" exp="" field="id_ogg"/>
    <constraint desc="" exp="" field="toponomastica"/>
    <constraint desc="" exp="" field="rifciv"/>
    <constraint desc="" exp="" field="oggetto"/>
    <constraint desc="" exp="" field="foto"/>
    <constraint desc="" exp="" field="data"/>
    <constraint desc="" exp="" field="nome"/>
    <constraint desc="" exp="" field="intervento"/>
    <constraint desc="" exp="" field="scadenza"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortOrder="0" actionWidgetStyle="dropDown" sortExpression="">
    <columns>
      <column width="-1" name="id_ogg" type="field" hidden="0"/>
      <column width="-1" name="toponomastica" type="field" hidden="0"/>
      <column width="-1" name="rifciv" type="field" hidden="0"/>
      <column width="-1" name="oggetto" type="field" hidden="0"/>
      <column width="-1" name="foto" type="field" hidden="0"/>
      <column width="-1" name="data" type="field" hidden="0"/>
      <column width="-1" name="nome" type="field" hidden="0"/>
      <column width="-1" name="intervento" type="field" hidden="0"/>
      <column width="-1" name="scadenza" type="field" hidden="0"/>
      <column width="-1" type="actions" hidden="1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
I moduli di QGIS possono avere una funzione Python che può essere chiamata quando un modulo viene aperto.

Usa questa funzione per aggiungere logica extra ai tuoi moduli.

Inserisci il nome della funzione nel campo "Funzione Python di avvio".

Segue un esempio:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
geom = feature.geometry()
control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="data" editable="1"/>
    <field name="data_e" editable="1"/>
    <field name="data_p" editable="1"/>
    <field name="data_s" editable="1"/>
    <field name="data_ve" editable="1"/>
    <field name="data_vs" editable="1"/>
    <field name="desc_e" editable="1"/>
    <field name="desc_p" editable="1"/>
    <field name="desc_s" editable="1"/>
    <field name="descrizione" editable="1"/>
    <field name="esito_ve" editable="1"/>
    <field name="fk_oggetto" editable="1"/>
    <field name="foto" editable="1"/>
    <field name="id_interv" editable="1"/>
    <field name="id_ogg" editable="1"/>
    <field name="intervento" editable="1"/>
    <field name="nome" editable="1"/>
    <field name="nome_e" editable="1"/>
    <field name="nome_p" editable="1"/>
    <field name="nome_s" editable="1"/>
    <field name="note_ve" editable="1"/>
    <field name="note_vs" editable="1"/>
    <field name="oggetto" editable="1"/>
    <field name="operatore" editable="1"/>
    <field name="rifciv" editable="1"/>
    <field name="ruolo" editable="1"/>
    <field name="scadenza" editable="1"/>
    <field name="situazione" editable="1"/>
    <field name="toponomastica" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="data" labelOnTop="0"/>
    <field name="data_e" labelOnTop="0"/>
    <field name="data_p" labelOnTop="0"/>
    <field name="data_s" labelOnTop="0"/>
    <field name="data_ve" labelOnTop="0"/>
    <field name="data_vs" labelOnTop="0"/>
    <field name="desc_e" labelOnTop="0"/>
    <field name="desc_p" labelOnTop="0"/>
    <field name="desc_s" labelOnTop="0"/>
    <field name="descrizione" labelOnTop="0"/>
    <field name="esito_ve" labelOnTop="0"/>
    <field name="fk_oggetto" labelOnTop="0"/>
    <field name="foto" labelOnTop="0"/>
    <field name="id_interv" labelOnTop="0"/>
    <field name="id_ogg" labelOnTop="0"/>
    <field name="intervento" labelOnTop="0"/>
    <field name="nome" labelOnTop="0"/>
    <field name="nome_e" labelOnTop="0"/>
    <field name="nome_p" labelOnTop="0"/>
    <field name="nome_s" labelOnTop="0"/>
    <field name="note_ve" labelOnTop="0"/>
    <field name="note_vs" labelOnTop="0"/>
    <field name="oggetto" labelOnTop="0"/>
    <field name="operatore" labelOnTop="0"/>
    <field name="rifciv" labelOnTop="0"/>
    <field name="ruolo" labelOnTop="0"/>
    <field name="scadenza" labelOnTop="0"/>
    <field name="situazione" labelOnTop="0"/>
    <field name="toponomastica" labelOnTop="0"/>
  </labelOnTop>
  <reuseLastValue>
    <field reuseLastValue="0" name="data"/>
    <field reuseLastValue="0" name="data_e"/>
    <field reuseLastValue="0" name="data_p"/>
    <field reuseLastValue="0" name="data_s"/>
    <field reuseLastValue="0" name="data_ve"/>
    <field reuseLastValue="0" name="data_vs"/>
    <field reuseLastValue="0" name="desc_e"/>
    <field reuseLastValue="0" name="desc_p"/>
    <field reuseLastValue="0" name="desc_s"/>
    <field reuseLastValue="0" name="descrizione"/>
    <field reuseLastValue="0" name="esito_ve"/>
    <field reuseLastValue="0" name="fk_oggetto"/>
    <field reuseLastValue="0" name="foto"/>
    <field reuseLastValue="0" name="id_interv"/>
    <field reuseLastValue="0" name="id_ogg"/>
    <field reuseLastValue="0" name="intervento"/>
    <field reuseLastValue="0" name="nome"/>
    <field reuseLastValue="0" name="nome_e"/>
    <field reuseLastValue="0" name="nome_p"/>
    <field reuseLastValue="0" name="nome_s"/>
    <field reuseLastValue="0" name="note_ve"/>
    <field reuseLastValue="0" name="note_vs"/>
    <field reuseLastValue="0" name="oggetto"/>
    <field reuseLastValue="0" name="operatore"/>
    <field reuseLastValue="0" name="rifciv"/>
    <field reuseLastValue="0" name="ruolo"/>
    <field reuseLastValue="0" name="scadenza"/>
    <field reuseLastValue="0" name="situazione"/>
    <field reuseLastValue="0" name="toponomastica"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"oggetto"</previewExpression>
  <mapTip enabled="1"></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
