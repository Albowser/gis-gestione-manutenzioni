<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis minScale="100000000" simplifyMaxScale="1" simplifyDrawingHints="0" readOnly="0" version="3.36.0-Maidenhead" simplifyDrawingTol="1" styleCategories="AllStyleCategories" maxScale="0" hasScaleBasedVisibilityFlag="0" labelsEnabled="0" simplifyAlgorithm="0" simplifyLocal="1" symbologyReferenceScale="-1">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal startField="" durationUnit="min" startExpression="" accumulate="0" fixedDuration="0" limitMode="0" endExpression="" mode="0" endField="" enabled="0" durationField="id_ogg">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation zoffset="0" binding="Centroid" extrusion="0" clamping="Terrain" type="IndividualFeatures" zscale="1" symbology="Line" respectLayerSymbol="1" extrusionEnabled="0" showMarkerSymbolInSurfacePlots="0">
    <data-defined-properties>
      <Option type="Map">
        <Option name="name" type="QString" value=""/>
        <Option name="properties"/>
        <Option name="type" type="QString" value="collection"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol clip_to_extent="1" alpha="1" name="" type="line" frame_rate="10" force_rhr="0" is_animated="0">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer id="{aff80c78-e84a-4bc8-b761-1509cfbc6798}" pass="0" locked="0" enabled="1" class="SimpleLine">
          <Option type="Map">
            <Option name="align_dash_pattern" type="QString" value="0"/>
            <Option name="capstyle" type="QString" value="square"/>
            <Option name="customdash" type="QString" value="5;2"/>
            <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="customdash_unit" type="QString" value="MM"/>
            <Option name="dash_pattern_offset" type="QString" value="0"/>
            <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
            <Option name="draw_inside_polygon" type="QString" value="0"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="line_color" type="QString" value="141,90,153,255,rgb:0.55294117647058827,0.35294117647058826,0.59999999999999998,1"/>
            <Option name="line_style" type="QString" value="solid"/>
            <Option name="line_width" type="QString" value="0.6"/>
            <Option name="line_width_unit" type="QString" value="MM"/>
            <Option name="offset" type="QString" value="0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="ring_filter" type="QString" value="0"/>
            <Option name="trim_distance_end" type="QString" value="0"/>
            <Option name="trim_distance_end_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_end_unit" type="QString" value="MM"/>
            <Option name="trim_distance_start" type="QString" value="0"/>
            <Option name="trim_distance_start_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_start_unit" type="QString" value="MM"/>
            <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
            <Option name="use_custom_dash" type="QString" value="0"/>
            <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol clip_to_extent="1" alpha="1" name="" type="fill" frame_rate="10" force_rhr="0" is_animated="0">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer id="{8a708fb9-5cd1-453f-9902-3fdccad6e033}" pass="0" locked="0" enabled="1" class="SimpleFill">
          <Option type="Map">
            <Option name="border_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="color" type="QString" value="141,90,153,255,rgb:0.55294117647058827,0.35294117647058826,0.59999999999999998,1"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="101,64,109,255,rgb:0.39494926375219347,0.25209430075532158,0.42856488899061568,1"/>
            <Option name="outline_style" type="QString" value="solid"/>
            <Option name="outline_width" type="QString" value="0.2"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="style" type="QString" value="solid"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol clip_to_extent="1" alpha="1" name="" type="marker" frame_rate="10" force_rhr="0" is_animated="0">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer id="{c04cb9d0-9eb9-4a31-99e7-d11dd9f5de4a}" pass="0" locked="0" enabled="1" class="SimpleMarker">
          <Option type="Map">
            <Option name="angle" type="QString" value="0"/>
            <Option name="cap_style" type="QString" value="square"/>
            <Option name="color" type="QString" value="141,90,153,255,rgb:0.55294117647058827,0.35294117647058826,0.59999999999999998,1"/>
            <Option name="horizontal_anchor_point" type="QString" value="1"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="name" type="QString" value="diamond"/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="101,64,109,255,rgb:0.39494926375219347,0.25209430075532158,0.42856488899061568,1"/>
            <Option name="outline_style" type="QString" value="solid"/>
            <Option name="outline_width" type="QString" value="0.2"/>
            <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="scale_method" type="QString" value="diameter"/>
            <Option name="size" type="QString" value="3"/>
            <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="size_unit" type="QString" value="MM"/>
            <Option name="vertical_anchor_point" type="QString" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 toleranceUnitScale="3x:0,0,0,0,0,0" type="pointCluster" tolerance="3" forceraster="0" toleranceUnit="MM" referencescale="-1" enableorderby="0" symbollevels="0">
    <renderer-v2 type="singleSymbol" forceraster="0" referencescale="-1" enableorderby="0" symbollevels="0">
      <symbols>
        <symbol clip_to_extent="1" alpha="1" name="0" type="marker" frame_rate="10" force_rhr="0" is_animated="0">
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
          <layer id="{1939ed62-a671-4f21-b726-cd756d7cbcbb}" pass="0" locked="0" enabled="1" class="SimpleMarker">
            <Option type="Map">
              <Option name="angle" type="QString" value="0"/>
              <Option name="cap_style" type="QString" value="square"/>
              <Option name="color" type="QString" value="0,168,20,255,hsv:0.3527777777777778,1,0.6588235294117647,1"/>
              <Option name="horizontal_anchor_point" type="QString" value="1"/>
              <Option name="joinstyle" type="QString" value="bevel"/>
              <Option name="name" type="QString" value="circle"/>
              <Option name="offset" type="QString" value="0,0"/>
              <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="offset_unit" type="QString" value="MM"/>
              <Option name="outline_color" type="QString" value="35,35,35,255,rgb:0.13725490196078433,0.13725490196078433,0.13725490196078433,1"/>
              <Option name="outline_style" type="QString" value="solid"/>
              <Option name="outline_width" type="QString" value="0"/>
              <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="outline_width_unit" type="QString" value="MM"/>
              <Option name="scale_method" type="QString" value="diameter"/>
              <Option name="size" type="QString" value="3"/>
              <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="size_unit" type="QString" value="MM"/>
              <Option name="vertical_anchor_point" type="QString" value="1"/>
            </Option>
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" type="QString" value=""/>
                <Option name="properties" type="Map">
                  <Option name="fillColor" type="Map">
                    <Option name="active" type="bool" value="true"/>
                    <Option name="expression" type="QString" value="case&#xa;when &quot;esito&quot; = 1&#xa;then color_rgb(0,255,0)&#xa;else color_rgb(255,0,0)&#xa;end"/>
                    <Option name="type" type="int" value="3"/>
                  </Option>
                </Option>
                <Option name="type" type="QString" value="collection"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </symbols>
      <rotation/>
      <sizescale/>
    </renderer-v2>
    <symbol clip_to_extent="1" alpha="1" name="centerSymbol" type="marker" frame_rate="10" force_rhr="0" is_animated="0">
      <data_defined_properties>
        <Option type="Map">
          <Option name="name" type="QString" value=""/>
          <Option name="properties"/>
          <Option name="type" type="QString" value="collection"/>
        </Option>
      </data_defined_properties>
      <layer id="{718f43ef-fc91-41fd-b7be-273687195357}" pass="0" locked="0" enabled="1" class="SimpleMarker">
        <Option type="Map">
          <Option name="angle" type="QString" value="0"/>
          <Option name="cap_style" type="QString" value="square"/>
          <Option name="color" type="QString" value="0,168,20,255,hsv:0.3527777777777778,1,0.6588235294117647,1"/>
          <Option name="horizontal_anchor_point" type="QString" value="1"/>
          <Option name="joinstyle" type="QString" value="bevel"/>
          <Option name="name" type="QString" value="circle"/>
          <Option name="offset" type="QString" value="0,0"/>
          <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          <Option name="offset_unit" type="QString" value="MM"/>
          <Option name="outline_color" type="QString" value="35,35,35,255,rgb:0.13725490196078433,0.13725490196078433,0.13725490196078433,1"/>
          <Option name="outline_style" type="QString" value="solid"/>
          <Option name="outline_width" type="QString" value="0"/>
          <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          <Option name="outline_width_unit" type="QString" value="MM"/>
          <Option name="scale_method" type="QString" value="diameter"/>
          <Option name="size" type="QString" value="4"/>
          <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          <Option name="size_unit" type="QString" value="MM"/>
          <Option name="vertical_anchor_point" type="QString" value="1"/>
        </Option>
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
      </layer>
      <layer id="{bc84f8df-b195-4b1b-903d-40f20b284d1e}" pass="0" locked="0" enabled="1" class="FontMarker">
        <Option type="Map">
          <Option name="angle" type="QString" value="0"/>
          <Option name="chr" type="QString" value="A"/>
          <Option name="color" type="QString" value="255,255,255,255,hsv:0,0,1,1"/>
          <Option name="font" type="QString" value="DejaVu Sans"/>
          <Option name="font_style" type="QString" value=""/>
          <Option name="horizontal_anchor_point" type="QString" value="1"/>
          <Option name="joinstyle" type="QString" value="miter"/>
          <Option name="offset" type="QString" value="0,-0.40000000000000002"/>
          <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          <Option name="offset_unit" type="QString" value="MM"/>
          <Option name="outline_color" type="QString" value="255,255,255,255,rgb:1,1,1,1"/>
          <Option name="outline_width" type="QString" value="0"/>
          <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          <Option name="outline_width_unit" type="QString" value="MM"/>
          <Option name="size" type="QString" value="3.2"/>
          <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          <Option name="size_unit" type="QString" value="MM"/>
          <Option name="vertical_anchor_point" type="QString" value="1"/>
        </Option>
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties" type="Map">
              <Option name="char" type="Map">
                <Option name="active" type="bool" value="true"/>
                <Option name="expression" type="QString" value="@cluster_size"/>
                <Option name="type" type="int" value="3"/>
              </Option>
            </Option>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
      </layer>
    </symbol>
  </renderer-v2>
  <selection mode="Default">
    <selectionColor invalid="1"/>
    <selectionSymbol>
      <symbol clip_to_extent="1" alpha="1" name="" type="marker" frame_rate="10" force_rhr="0" is_animated="0">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer id="{eee0c992-991f-484c-bc0b-2e851d289254}" pass="0" locked="0" enabled="1" class="SimpleMarker">
          <Option type="Map">
            <Option name="angle" type="QString" value="0"/>
            <Option name="cap_style" type="QString" value="square"/>
            <Option name="color" type="QString" value="255,0,0,255,rgb:1,0,0,1"/>
            <Option name="horizontal_anchor_point" type="QString" value="1"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="name" type="QString" value="circle"/>
            <Option name="offset" type="QString" value="0,0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="outline_color" type="QString" value="35,35,35,255,rgb:0.13725490196078433,0.13725490196078433,0.13725490196078433,1"/>
            <Option name="outline_style" type="QString" value="solid"/>
            <Option name="outline_width" type="QString" value="0"/>
            <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="outline_width_unit" type="QString" value="MM"/>
            <Option name="scale_method" type="QString" value="diameter"/>
            <Option name="size" type="QString" value="2"/>
            <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="size_unit" type="QString" value="MM"/>
            <Option name="vertical_anchor_point" type="QString" value="1"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </selectionSymbol>
  </selection>
  <customproperties>
    <Option type="Map">
      <Option name="QFieldSync/action" type="QString" value="no_action"/>
      <Option name="QFieldSync/attachment_naming" type="QString" value="{}"/>
      <Option name="QFieldSync/cloud_action" type="QString" value="no_action"/>
      <Option name="QFieldSync/geometry_locked_expression" type="QString" value=""/>
      <Option name="QFieldSync/photo_naming" type="QString" value="{}"/>
      <Option name="QFieldSync/relationship_maximum_visible" type="QString" value="{}"/>
      <Option name="QFieldSync/tracking_distance_requirement_minimum_meters" type="int" value="30"/>
      <Option name="QFieldSync/tracking_erroneous_distance_safeguard_maximum_meters" type="int" value="1"/>
      <Option name="QFieldSync/tracking_measurement_type" type="int" value="0"/>
      <Option name="QFieldSync/tracking_time_requirement_interval_seconds" type="int" value="30"/>
      <Option name="embeddedWidgets/count" type="int" value="0"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory enabled="0" height="15" opacity="1" backgroundColor="#ffffff" barWidth="5" minScaleDenominator="0" diagramOrientation="Up" maxScaleDenominator="1e+08" penColor="#000000" rotationOffset="270" sizeType="MM" penWidth="0" spacingUnitScale="3x:0,0,0,0,0,0" penAlpha="255" spacingUnit="MM" lineSizeScale="3x:0,0,0,0,0,0" scaleDependency="Area" minimumSize="0" backgroundAlpha="255" showAxis="1" direction="0" scaleBasedVisibility="0" lineSizeType="MM" sizeScale="3x:0,0,0,0,0,0" width="15" labelPlacementMethod="XHeight" spacing="5">
      <fontProperties bold="0" style="" description="Ubuntu,10,-1,5,50,0,0,0,0,0" italic="0" underline="0" strikethrough="0"/>
      <attribute colorOpacity="1" label="" color="#000000" field=""/>
      <axisSymbol>
        <symbol clip_to_extent="1" alpha="1" name="" type="line" frame_rate="10" force_rhr="0" is_animated="0">
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
          <layer id="{a707f453-6838-4a4e-88bb-b17a01be7b28}" pass="0" locked="0" enabled="1" class="SimpleLine">
            <Option type="Map">
              <Option name="align_dash_pattern" type="QString" value="0"/>
              <Option name="capstyle" type="QString" value="square"/>
              <Option name="customdash" type="QString" value="5;2"/>
              <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="customdash_unit" type="QString" value="MM"/>
              <Option name="dash_pattern_offset" type="QString" value="0"/>
              <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
              <Option name="draw_inside_polygon" type="QString" value="0"/>
              <Option name="joinstyle" type="QString" value="bevel"/>
              <Option name="line_color" type="QString" value="35,35,35,255,rgb:0.13725490196078433,0.13725490196078433,0.13725490196078433,1"/>
              <Option name="line_style" type="QString" value="solid"/>
              <Option name="line_width" type="QString" value="0.26"/>
              <Option name="line_width_unit" type="QString" value="MM"/>
              <Option name="offset" type="QString" value="0"/>
              <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="offset_unit" type="QString" value="MM"/>
              <Option name="ring_filter" type="QString" value="0"/>
              <Option name="trim_distance_end" type="QString" value="0"/>
              <Option name="trim_distance_end_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="trim_distance_end_unit" type="QString" value="MM"/>
              <Option name="trim_distance_start" type="QString" value="0"/>
              <Option name="trim_distance_start_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="trim_distance_start_unit" type="QString" value="MM"/>
              <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
              <Option name="use_custom_dash" type="QString" value="0"/>
              <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            </Option>
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" type="QString" value=""/>
                <Option name="properties"/>
                <Option name="type" type="QString" value="collection"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings zIndex="0" linePlacementFlags="18" showAll="1" placement="0" priority="0" obstacle="0" dist="0">
    <properties>
      <Option type="Map">
        <Option name="name" type="QString" value=""/>
        <Option name="properties"/>
        <Option name="type" type="QString" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend showLabelLegend="0" type="default-vector"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field configurationFlags="NoFlag" name="id_ogg">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="toponomastica">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="rifciv">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="oggetto">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="foto">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="data">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="verificatore">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="note">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="esito">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" index="0" field="id_ogg"/>
    <alias name="" index="1" field="toponomastica"/>
    <alias name="" index="2" field="rifciv"/>
    <alias name="" index="3" field="oggetto"/>
    <alias name="" index="4" field="foto"/>
    <alias name="" index="5" field="data"/>
    <alias name="" index="6" field="verificatore"/>
    <alias name="" index="7" field="note"/>
    <alias name="" index="8" field="esito"/>
  </aliases>
  <splitPolicies>
    <policy field="id_ogg" policy="Duplicate"/>
    <policy field="toponomastica" policy="Duplicate"/>
    <policy field="rifciv" policy="Duplicate"/>
    <policy field="oggetto" policy="Duplicate"/>
    <policy field="foto" policy="Duplicate"/>
    <policy field="data" policy="Duplicate"/>
    <policy field="verificatore" policy="Duplicate"/>
    <policy field="note" policy="Duplicate"/>
    <policy field="esito" policy="Duplicate"/>
  </splitPolicies>
  <defaults>
    <default expression="" applyOnUpdate="0" field="id_ogg"/>
    <default expression="" applyOnUpdate="0" field="toponomastica"/>
    <default expression="" applyOnUpdate="0" field="rifciv"/>
    <default expression="" applyOnUpdate="0" field="oggetto"/>
    <default expression="" applyOnUpdate="0" field="foto"/>
    <default expression="" applyOnUpdate="0" field="data"/>
    <default expression="" applyOnUpdate="0" field="verificatore"/>
    <default expression="" applyOnUpdate="0" field="note"/>
    <default expression="" applyOnUpdate="0" field="esito"/>
  </defaults>
  <constraints>
    <constraint unique_strength="0" constraints="0" notnull_strength="0" field="id_ogg" exp_strength="0"/>
    <constraint unique_strength="0" constraints="0" notnull_strength="0" field="toponomastica" exp_strength="0"/>
    <constraint unique_strength="0" constraints="0" notnull_strength="0" field="rifciv" exp_strength="0"/>
    <constraint unique_strength="0" constraints="0" notnull_strength="0" field="oggetto" exp_strength="0"/>
    <constraint unique_strength="0" constraints="0" notnull_strength="0" field="foto" exp_strength="0"/>
    <constraint unique_strength="0" constraints="0" notnull_strength="0" field="data" exp_strength="0"/>
    <constraint unique_strength="0" constraints="0" notnull_strength="0" field="verificatore" exp_strength="0"/>
    <constraint unique_strength="0" constraints="0" notnull_strength="0" field="note" exp_strength="0"/>
    <constraint unique_strength="0" constraints="0" notnull_strength="0" field="esito" exp_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" exp="" field="id_ogg"/>
    <constraint desc="" exp="" field="toponomastica"/>
    <constraint desc="" exp="" field="rifciv"/>
    <constraint desc="" exp="" field="oggetto"/>
    <constraint desc="" exp="" field="foto"/>
    <constraint desc="" exp="" field="data"/>
    <constraint desc="" exp="" field="verificatore"/>
    <constraint desc="" exp="" field="note"/>
    <constraint desc="" exp="" field="esito"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortOrder="0" actionWidgetStyle="dropDown" sortExpression="">
    <columns>
      <column width="-1" name="id_ogg" type="field" hidden="0"/>
      <column width="-1" name="toponomastica" type="field" hidden="0"/>
      <column width="-1" name="rifciv" type="field" hidden="0"/>
      <column width="-1" name="oggetto" type="field" hidden="0"/>
      <column width="-1" name="foto" type="field" hidden="0"/>
      <column width="-1" name="data" type="field" hidden="0"/>
      <column width="-1" name="verificatore" type="field" hidden="0"/>
      <column width="-1" name="note" type="field" hidden="0"/>
      <column width="-1" name="esito" type="field" hidden="0"/>
      <column width="-1" type="actions" hidden="1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
I moduli di QGIS possono avere una funzione Python che può essere chiamata quando un modulo viene aperto.

Usa questa funzione per aggiungere logica extra ai tuoi moduli.

Inserisci il nome della funzione nel campo "Funzione Python di avvio".

Segue un esempio:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
geom = feature.geometry()
control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="data" editable="1"/>
    <field name="descrizione" editable="1"/>
    <field name="esito" editable="1"/>
    <field name="foto" editable="1"/>
    <field name="id_ogg" editable="1"/>
    <field name="note" editable="1"/>
    <field name="oggetto" editable="1"/>
    <field name="operatore" editable="1"/>
    <field name="rifciv" editable="1"/>
    <field name="ruolo" editable="1"/>
    <field name="situazione" editable="1"/>
    <field name="toponomastica" editable="1"/>
    <field name="verificatore" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="data" labelOnTop="0"/>
    <field name="descrizione" labelOnTop="0"/>
    <field name="esito" labelOnTop="0"/>
    <field name="foto" labelOnTop="0"/>
    <field name="id_ogg" labelOnTop="0"/>
    <field name="note" labelOnTop="0"/>
    <field name="oggetto" labelOnTop="0"/>
    <field name="operatore" labelOnTop="0"/>
    <field name="rifciv" labelOnTop="0"/>
    <field name="ruolo" labelOnTop="0"/>
    <field name="situazione" labelOnTop="0"/>
    <field name="toponomastica" labelOnTop="0"/>
    <field name="verificatore" labelOnTop="0"/>
  </labelOnTop>
  <reuseLastValue>
    <field reuseLastValue="0" name="data"/>
    <field reuseLastValue="0" name="descrizione"/>
    <field reuseLastValue="0" name="esito"/>
    <field reuseLastValue="0" name="foto"/>
    <field reuseLastValue="0" name="id_ogg"/>
    <field reuseLastValue="0" name="note"/>
    <field reuseLastValue="0" name="oggetto"/>
    <field reuseLastValue="0" name="operatore"/>
    <field reuseLastValue="0" name="rifciv"/>
    <field reuseLastValue="0" name="ruolo"/>
    <field reuseLastValue="0" name="situazione"/>
    <field reuseLastValue="0" name="toponomastica"/>
    <field reuseLastValue="0" name="verificatore"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"descrizione"</previewExpression>
  <mapTip enabled="1"></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
